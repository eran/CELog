// =================================================================================================
//
//	CopyEngine Framework
//	Copyright 2012 Eran. All Rights Reserved.
//
//	This program is free software. You can redistribute and/or modify it
//	in accordance with the terms of the accompanying license agreement.
//
// =================================================================================================

package copyengine.utils.debug
{
    public class TraceLog implements IDebugLog
    {
        public function TraceLog()
        {
        }

        public function log(...args):void
        {
            trace(args);
        }

        public function err(...args):void
        {
            trace(args);
        }

        public function warn(...args):void
        {
            trace(args);
        }

    }
}
