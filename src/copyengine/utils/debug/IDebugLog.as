package copyengine.utils.debug
{
    public interface IDebugLog
    {
        function log(...args):void;

        function err(...args):void;

        function warn(...args):void;
    }
}