package copyengine.utils.debug
{


	public class CELog
	{
		private static var logImpl:IDebugLog;

		public static function setLogImpl(_logImpl:IDebugLog):void
		{
			logImpl=_logImpl;
		}

		public static function log(_msg:String):void
		{
			logImpl && logImpl.log(_msg);
		}

		public static function warn(_msg:String):void
		{
			logImpl && logImpl.warn(_msg);
		}

		public static function err(_msg:String):void
		{
			logImpl && logImpl.err(_msg);
		}

	}
}
